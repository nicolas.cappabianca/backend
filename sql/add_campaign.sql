INSERT OR ROLLBACK INTO campaigns
(
	starts,
	expires
)
VALUES
(
	:starts,
	:expires
)
